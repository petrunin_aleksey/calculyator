package ru.pae.calculyator;

import java.util.Scanner;

public class Calculyator {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите выражение: ");
        String expression = scanner.nextLine();
        int result = 0;
        String array[] = expression.split(" ");
        int number1 = Integer.valueOf(array[0]);
        int number2 = Integer.valueOf(array[2]);

        switch (array[1]) {
            case "+":
                result = MathInt.add(number1, number2);
                System.out.printf("Ответ: \n" + number1 + " " + array[1] + " " + number2 + " = " + result);
                break;
            case "-":
                result = MathInt.sub(number1, number2);
                System.out.printf("Ответ: \n" + number1 + " " + array[1] + " " + number2 + " = " + result);
                break;
            case "/":
                result = MathInt.div(number1, number2);
                System.out.printf("Ответ: \n" + number1 + " " + array[1] + " " + number2 + " = " + result);
                break;

            case "*":
                result = MathInt.mult(number1, number2);
                System.out.printf("Ответ: \n" + number1 + " " + array[1] + " " + number2 + " = " + result);
                break;

            case "^":
                result = MathInt.exp(number1, number2);
                System.out.printf("Ответ: \n" + number1 + " " + array[1] + " " + number2 + " = " + result);
                break;

            case "%":
                result = MathInt.rem(number1, number2);
                System.out.printf("Ответ: \n" + number1 + " " + array[1] + " " + number2 + " = " + result);
                break;
            default:
                System.out.println("Введен не верный знак операции!");

        }
    }
}