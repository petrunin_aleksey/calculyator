package ru.pae.calculyator;

public class MathInt {
    public static int add(int number1,int number2) {
        int result = number1 + number2 ;
        return result;
    }

    public static int sub(int number1,int number2) {
        int result = number1 - number2 ;
        return result;
    }

    public static int div(int number1,int number2) {
        int result = number1 / number2 ;
        return result;
    }

    public static int mult(int number1,int number2) {
        int result = number1 * number2 ;
        return result;
    }

    public static int exp(int number1, int number2) {
        int result = 1;
        for (int i = 0; i < number2; i++) {
            result *= number1;
        }
        return result;
    }

    public static int rem(int number1,int number2) {
        int result =number1 % number2 ;
        return result;
    }
}

